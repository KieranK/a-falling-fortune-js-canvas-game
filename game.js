// System Variables
var canvas;
var g;
var renderCanvasInterval = null;

var gamePaused = false;
var gameInterval;
var gameLevel = 1;
var gameInMenu = true;
var playerDead = false;

//Highscore code: http://stackoverflow.com/questions/29370017/adding-a-high-score-to-local-storage
var playerScore = 0;
var highScore = localStorage.getItem("highScore");


var rightKey = false,
    leftKey = false,
    upKey = false,
    downKey = false;

var backgroundX = 0;
var backgroundY = 0;
var backgroundY2 = 500;
//Player sprite from http://www.nesmaps.com/maps/RushnAttack/sprites/RushnAttackSprites.html
var playerSprite = new Image();
playerSprite.src = "./images/player.png";
//Created with photoshop
var background = new Image();
background.src = "./images/Sky.jpg";
//Edited with photoshop
var money = new Image();
money.src = "./images/cash.png";
//Bird sprite from http://opengameart.org/content/free-game-asset-grumpy-flappy-bird-sprite-sheets
var obstacle = new Image();
obstacle.src = "./images/bird.png";

// OnAllAssetsLoaded from derek.dkit.ie
window.onload = onAllAssetsLoaded;
document.write("<div id='loadingMessage'>Game is Loading..</div>");
function onAllAssetsLoaded()
{
    // hide the webpage loading message
    document.getElementById('loadingMessage').style.visibility = "hidden";
    canvas = document.getElementById("canvas");
    g = canvas.getContext("2d");
    canvas.width = CANVAS_WIDTH;
    canvas.height = CANVAS_HEIGHT;
    g.font="20px Arial";
    g.fillStyle = "rgba(32, 45, 21, 0.6)";
    //Event Listeners
    document.addEventListener('keydown', keyDown, false);
    document.addEventListener('keyup', keyUp, false);
    playerDead = false;
    //Welcome Message
    drawBackground();
    g.beginPath();
    g.font="30px Arial";
    g.fillText("Welcome to A Falling Fortune!", 200, 220);
    g.closePath();
    g.beginPath();
    g.font="20px Arial";
    g.fillText("To begin press the space bar", 260, 300);
    g.closePath(); 
}
function startGame()
{
    clearCanvas();
    drawBackground();
    setPlayerY(250);
    gameInterval = setInterval(renderCanvas, TIME_PER_FRAME);
    gameInMenu = false;
}
//FPS code from http://stackoverflow.com/questions/4787431/check-fps-in-js
// FPS related variables
var filterStrength = 20;
var frameTime = 0, lastLoop = new Date, thisLoop, fps;
// Game Loop
function renderCanvas()
{
    clearCanvas(); //Clears the previous loop
    drawBackground(); //Draws the background image
    drawCash(); //Draws the floating cash
    drawBird(); //Draws the bird obstacles
    drawPlayer(); //Draws the player
    //Indicates what level the player is on
    if(gameLevel === 1)
    {
        drawLevelOne();
        calcTime();
    }
    else if(gameLevel === 2)
    {
        drawLevelTwo();
        calcTime();
    }
    else if(gameLevel === 3)
    {
        drawLevelThree();
        calcTime();
    }
    //Player Score
    g.fillText("$: "+playerScore, 10, 50);
    //Calculating FPS
    var thisFrameTime = (thisLoop=new Date) - lastLoop;
    frameTime+= (thisFrameTime - frameTime) / filterStrength;
    lastLoop = thisLoop;
    setInterval(calcFPS ,1000);
    g.fillText("FPS: "+fps, 10, 20);
    // displays a warning when FPS drops below 30
    if(fps < 30)
    {   //If the FPS drops below 30, a warning message appears below the canvas
        document.getElementById("fps").style.visibility = "visible";
        document.getElementById("fps").style.color = "red";
        document.getElementById("fps").innerHTML = "Low FPS!";
    }
    if(fps > 30)
    {   //Warning message dissapears if the FPS is above 30
        document.getElementById("fps").style.visibility = "hidden";
    }
}
/** Pausing game **/
function pauseGame() 
{
  if (gamePaused === false) 
  {
    g.fillText("Game Paused", 350, 250);
    gamePaused = true;
    clearInterval(gameInterval);
    console.log("Game has just been paused");
  }
  }
function unPauseGame()
{
    if(gamePaused === true){
        gamePaused = false;
        gameInterval = setInterval(renderCanvas, TIME_PER_FRAME);
        console.log("Game has just been unpaused");
    }
}
function togglePause()
 {
     if(gamePaused === false)
     {
         pauseGame();
     }
     else
     {
        unPauseGame();
     }    
 }
//Window loosing focus
window.onblur = function()
{
   if(gamePaused === false && gameInMenu === false){
       pauseGame();
   }
};
//Window regains focus
window.onfocus = function()
{
   if(gamePaused === true && gameInMenu === false){
       gameCountdown();
   }
};
function gameCountdown() //Function adapted from http://www.ibm.com/developerworks/library/j-html5-game3/
{
    displayToast('3'); // Display 3 for one half second

      setTimeout(function (e) {
         
         displayToast('2'); // Display 2 for one half second

         setTimeout(function (e) {
            displayToast('1'); // Display 1 for one half second

            setTimeout(function (e) {
               unPauseGame();

               setTimeout(function (e) { // Wait for '1' to disappear
                  
               }, 2000);
            }, 1000);
         }, 1000);
      }, 1000);
   }
function displayToast(text)
{
    console.log(text);
    if(text == "3")
    {   
        g.fillText("3..", 250, 250);
    }
    if(text == "2")
    {
        g.fillText("2..", 250, 270);
    }
    if(text == "1")
    {
        g.fillText("1..", 250, 290);
    }
}
/** FPS **/
function calcFPS()
{
    fps = (1000/frameTime).toFixed(1);
}
/** Input Handling **/
function keyDown(e) 
{
  if (e.keyCode === 39) rightKey = true;
  else if (e.keyCode === 37) leftKey = true;
  if (e.keyCode === 38) upKey = true;
  else if (e.keyCode === 40) downKey = true;
  if (e.keyCode === 80) togglePause();
  if(gameInMenu === true && gameLevel === 1) //Game in start menu
  {
      if(e.keyCode === 32) startGame(); //Space bar
  }
  if(gameInMenu === true && gameLevel === 2) //Level one Complete
  {
       if(e.keyCode === 32) startGame(); //Space bar   
  }
  if(gameInMenu === true && gameLevel === 3) //Level Two Complete
  {
       if(e.keyCode === 32) startGame(); //Space bar
  }
  if(playerDead === true) //Player has died
  {
       if(e.keyCode === 32) location.reload(); //Space bar
  }
}
function keyUp(e) 
{
  if (e.keyCode === 39) rightKey = false;
  else if (e.keyCode === 37) leftKey = false;
  if (e.keyCode === 38) upKey = false;
  else if (e.keyCode === 40) downKey = false;
}
/** Player **/ 
var player = {
    playerX: 400,
    playerY: 300,
    playerWidth: 40,
    playerHeight: 60,
    PlayerMoveSpeed: 2,
    friction: 0.98,
    velX: 0,
    velY: 0   
};
function Player()
{
    this.playerX = 400;
    this.playerY = 300;
    this.playerWidth = 40;
    this.playerHeight = 60;
    this.PlayerMoveSpeed = 1;
    this.friction = 0.98;
    this.velX = 0;
    this.velY = 0;
}
var Player = new Player();   
function drawPlayer()
{
    if (upKey) // up
    {
        if(player.velY > -player.PlayerMoveSpeed)
        {
            player.velY = player.velY - .4;
        }
    }
    else if (downKey) // down
    {
        if(player.velY < player.PlayerMoveSpeed)
        {
            player.velY+=5;
        }
    }
    else if (rightKey) // right
    {
        if(player.velX < player.PlayerMoveSpeed)
        {
            player.velX+=5;
        }
    }
    else if (leftKey)  // left
    {
        if(player.velX > -player.PlayerMoveSpeed)
        {
            player.velX-=5;
        }
    } 
    player.velY *= player.friction;
    player.playerY += player.velY;
    
    player.velX *= player.friction;
    player.playerX += player.velX;
    //Drawing player
    g.drawImage(playerSprite, player.playerX, player.playerY, player.playerWidth, player.playerHeight);
}
function setPlayerY(y)
{
    player.playerY = y;
}
/* Cash */
var cash = {
    cashX1: (Math.floor(Math.random() * 800) + 1),
    cashX2: (Math.floor(Math.random() * 800) + 1),
    cashX3: (Math.floor(Math.random() * 800) + 1),
    cashX4: (Math.floor(Math.random() * 800) + 1),
    cashY1: 499,
    cashY2: 499,
    cashY3: 499,
    cashY4: 499,
    cashWidth: 20,
    cashHeight: 30,
    cashMoveSpeed: 2    
};
function Cash()
{
    this.cashX1 = (Math.floor(Math.random() * 800) + 1);
    this.cashX2 = (Math.floor(Math.random() * 800) + 1);
    this.cashX3 = (Math.floor(Math.random() * 800) + 1);
    this.cashX4 = (Math.floor(Math.random() * 800) + 1);
    this.cashY1 = 499;
    this.cashY2 = 499;
    this.cashY3 = 499;
    this.cashY4 = 499;
    this.cashWidth = 20;
    this.cashHeight = 30;
    this.cashMoveSpeed = 2;
}
var Cash = new Cash(); 
//Generates random starting point for cash
var startPos1 = (Math.floor(Math.random() * 499) + 400);
var startPos2 = (Math.floor(Math.random() * 499) + 400);
var startPos3 = (Math.floor(Math.random() * 499) + 400);
var startPos4 = (Math.floor(Math.random() * 499) + 400);
function drawCash()
{
    //Setting cash start position
    this.cashY1 = startPos1;
    this.cashY2 = startPos2;
    this.cashY3 = startPos3;
    this.cashY4 = startPos4;
    //Setting cash movement speed
    startPos1 = startPos1 - cash.cashMoveSpeed;
    startPos2 = startPos2 - cash.cashMoveSpeed;
    startPos3 = startPos3 - cash.cashMoveSpeed;
    startPos4 = startPos4 - cash.cashMoveSpeed;
    //Rendering Cash
    g.drawImage(money, cash.cashX1, this.cashY1, cash.cashWidth, cash.cashHeight);
    g.drawImage(money, cash.cashX2, this.cashY2, cash.cashWidth, cash.cashHeight);
    g.drawImage(money, cash.cashX3, this.cashY3, cash.cashWidth, cash.cashHeight);
    g.drawImage(money, cash.cashX4, this.cashY4, cash.cashWidth, cash.cashHeight);
    //When cash gets the top of the canvas, it is moved back down to the bottom
    if((this.cashY1)< -10)
    {
       resetCash1();;
    }
    if((this.cashY2)< -10)
    {
        resetCash2();
    }
    if((this.cashY3)< -10)
    {
        resetCash3();
    }
    if((this.cashY4)< -10)
    {
        resetCash4();
    }
    //Player hit detection
    if((player.playerX<(cash.cashX1+cash.cashWidth))&&((player.playerX+player.playerWidth)>cash.cashX1)&&(player.playerY<(this.cashY1+cash.cashHeight))&&((player.playerY+player.playerHeight)>this.cashY1))
    {
       console.log("cash1 hit");
       resetCash1();
       updateScore();
    }
    if((player.playerX<(cash.cashX2+cash.cashWidth))&&((player.playerX+player.playerWidth)>cash.cashX2)&&(player.playerY<(this.cashY2+cash.cashHeight))&&((player.playerY+player.playerHeight)>this.cashY2))
    {
       console.log("cash2 hit");
       resetCash2();
       updateScore();
    }
    if((player.playerX<(cash.cashX3+cash.cashWidth))&&((player.playerX+player.playerWidth)>cash.cashX3)&&(player.playerY<(this.cashY3+cash.cashHeight))&&((player.playerY+player.playerHeight)>this.cashY3))
    {
       console.log("cash3 hit");
       resetCash3();
       updateScore();
    }
    if((player.playerX<(cash.cashX4+cash.cashWidth))&&((player.playerX+player.playerWidth)>cash.cashX4)&&(player.playerY<(this.cashY4+cash.cashHeight))&&((player.playerY+player.playerHeight)>this.cashY4))
    {
       console.log("cash4 hit");
       resetCash4();
       updateScore();
    }
}
function resetCash1()
{
     startPos1 = 499;
     cash.cashX1 = (Math.floor(Math.random() * 800) + 1);
}
function resetCash2()
{
    startPos2 = 499;
    cash.cashX2 = (Math.floor(Math.random() * 800) + 1);
}
function resetCash3()
{
    startPos3 = 499;
    cash.cashX3 = (Math.floor(Math.random() * 800) + 1);
}
function resetCash4()
{
    startPos4 = 499;
    cash.cashX4 = (Math.floor(Math.random() * 800) + 1);
}
/* Bird */
var bird = {
    birdX1: 0,
    birdX2: 0,
    birdX3: 0,
    birdY1: 0,
    birdY2: 0,
    birdY3: 0,
    birdWidth: 30,
    birdHeight: 30,
    birdMoveSpeed: 2    
};
function Bird()
{
    this.birdX1 = 0;
    this.birdX2 = 0;
    this.birdX3 = 0;
    this.birdY1 = 0;
    this.birdY2 = 0;
    this.birdY3 = 0;
    this.birdWidth = 20;
    this.birdHeight = 30;
    this.birdMoveSpeed = 2;
}
var Bird = new Bird(); 
//Setting starting Y position to be between 490 and 5
var birdStartPos1 = (Math.floor(Math.random() * 490) + 5);
var birdStartPos2 = (Math.floor(Math.random() * 490) + 5);
var birdStartPos3 = (Math.floor(Math.random() * 490) + 5);
//Setting starting X position to be between -10 and 1
var birdStartX1 = ((Math.floor(Math.random() * 10) + 1)-10);
var birdStartX2 = ((Math.floor(Math.random() * 10) + 1)-10);
var birdStartX3 = ((Math.floor(Math.random() * 10) + 1)-10);
function drawBird()
{
    this.birdY1 = birdStartPos1;
    this.birdY2 = birdStartPos2;
    this.birdY3 = birdStartPos3;
    this.birdX1 = birdStartX1;
    this.birdX2 = birdStartX2;
    this.birdX3 = birdStartX3;
    if(gameLevel === 1)
    {
    g.drawImage(obstacle, this.birdX1, this.birdY1, bird.birdWidth, bird.birdHeight);
    birdStartX1 = birdStartX1 + bird.birdMoveSpeed;
    }
    if(gameLevel === 2)
    {
    g.drawImage(obstacle, this.birdX1, this.birdY1, bird.birdWidth, bird.birdHeight);
    g.drawImage(obstacle, this.birdX2, this.birdY2, bird.birdWidth, bird.birdHeight);
    birdStartX1 = birdStartX1 + bird.birdMoveSpeed;
    birdStartX2 = birdStartX2 + bird.birdMoveSpeed;
    }
    if(gameLevel === 3)
    {
    g.drawImage(obstacle, this.birdX1, this.birdY1, bird.birdWidth, bird.birdHeight);
    g.drawImage(obstacle, this.birdX2, this.birdY2, bird.birdWidth, bird.birdHeight);
    g.drawImage(obstacle, this.birdX3, this.birdY3, bird.birdWidth, bird.birdHeight);
    birdStartX1 = birdStartX1 + bird.birdMoveSpeed;
    birdStartX2 = birdStartX2 + bird.birdMoveSpeed;
    birdStartX3 = birdStartX3 + bird.birdMoveSpeed;
    }
    if((birdStartX1) > 800)
    {
        resetBird1();
    }
    if((birdStartX2) > 800)
    {
        resetBird2();
    }
    if((birdStartX3) > 800)
    {
        resetBird3();
    }
    if((player.playerX<(this.birdX1+bird.birdWidth))&&((player.playerX+player.playerWidth)>this.birdX1)&&(player.playerY<(this.birdY1+bird.birdHeight))&&((player.playerY+player.playerHeight)>this.birdY1))
    {
       console.log("bird1 hit");
       deathScreen();
    }
    if((player.playerX<(this.birdX2+bird.birdWidth))&&((player.playerX+player.playerWidth)>this.birdX2)&&(player.playerY<(this.birdY2+bird.birdHeight))&&((player.playerY+player.playerHeight)>this.birdY2))
    {
       console.log("bird2 hit");
       deathScreen();
    }
    if((player.playerX<(this.birdX3+bird.birdWidth))&&((player.playerX+player.playerWidth)>this.birdX3)&&(player.playerY<(this.birdY3+bird.birdHeight))&&((player.playerY+player.playerHeight)>this.birdY3))
    {
       console.log("bird3 hit");
       deathScreen();
    }
}
function resetBird1()
{
    birdStartX1 = ((Math.floor(Math.random() * 10) + 1)-10);;
    birdStartPos1 = (Math.floor(Math.random() * 490) + 5);
}
function resetBird2()
{
    birdStartX2 = ((Math.floor(Math.random() * 10) + 1)-10);;
    birdStartPos2 = (Math.floor(Math.random() * 490) + 5);
}
function resetBird3()
{
    birdStartX3 = ((Math.floor(Math.random() * 10) + 1)-10);;
    birdStartPos3 = (Math.floor(Math.random() * 490) + 5);
}
function resetCash()
{
    console.log("reset cash");
    resetCash1();
    resetCash2();
    resetCash3();
    resetCash4();
}
function resetBirds()
{
    console.log("reset Birds");
    resetBird1();
    resetBird2();
    resetBird3();
}
function updateScore()
{
    if(gameLevel ===1)
    {
        playerScore = playerScore + 10;
    }
    if(gameLevel ===2)
    {
        playerScore = playerScore + 20;
    }
    else
    {
        playerScore = playerScore + 50;
    }
   if(highScore !== null)
   {
        if (playerScore > highScore) 
        {
            localStorage.setItem("highScore", playerScore);
        }
    }
    else
    {
        localStorage.setItem("highScore", playerScore);
    }
}
// Clear Canvas
function clearCanvas()
{
    g.clearRect(0, 0, canvas.width, canvas.height);
}
/* Background */
function drawBackground()
{
    g.drawImage(background, backgroundX, backgroundY, CANVAS_WIDTH, CANVAS_HEIGHT);
    g.drawImage(background, backgroundX, backgroundY2, CANVAS_WIDTH, CANVAS_HEIGHT);
    
    if (backgroundY < -500) 
    {
        backgroundY = 499;
    }
    if (backgroundY2 < -500) 
    {
        backgroundY2 = 499;
    }
    backgroundY -= .5;
    backgroundY2 -= .5;
}
var timer = 0;
var time = 0;
function resetTime()
{
    timer = 0;
    time = 0;
}
function calcTime()
{
    timer = (timer + (frameTime/1000));
    time = Math.round(timer*10)/10;
    g.fillText("Time: "+time, 700, 20);
}
//Level One
function drawLevelOne()
{
    g.fillText("Level One", 340, 20);
    if(time >= 20)
    {
        resetCash();
        resetBirds();
        gameInMenu = true;
        clearInterval(gameInterval);
        clearCanvas();
        drawBackground();
        g.fillText("Level One Complete", 250, 250);
        g.fillText("Press space to start level two!", 250, 300);
        levelOneComplete = true;
        resetTime();
        gameLevel = 2;
    }
}
//Level Two
function drawLevelTwo()
{
    g.fillText("Level Two", 340, 20);
    if(time >= 30)
    {
       resetCash();
       resetBirds();
       gameInMenu = true;
       clearInterval(gameInterval);
       clearCanvas();
       drawBackground();
       g.fillText("Level Two Complete", 250, 250);
       g.fillText("Press space to start level three!", 250, 300);
       resetTime();
       gameLevel = 3;
    }
}
//Level Three
function drawLevelThree()
{
    g.fillText("Level Three", 340, 20);
    if(time >= 30)
    {
       if(playerScore > highScore)
       {
           g.fillText("New HighScore!", 240, 600);
           localStorage.setItem("highScore", playerScore);
       }
       resetCash();
       resetBirds();
       gameInMenu = true;
       clearInterval(gameInterval);
       clearCanvas();
       drawBackground();
       g.fillText("Level Three Complete", 250, 150);
       g.fillText("Your total score is: "+playerScore, 100, 300);
       g.fillText("Highscore: "+highScore, 400, 300);
       if(playerScore > highScore)
       {
           g.fillText("New HighScore!", 240, 600);
           localStorage.setItem("highScore", playerScore);
       }
       resetTime();
       gameLevel = 4;
    }
}
function deathScreen()
{
    playerDead = true;
    clearInterval(gameInterval);
    clearCanvas();
    drawBackground();
    g.fillText("You have died! (Avoid the birds!)", 250, 250);
}
